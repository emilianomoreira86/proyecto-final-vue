import { initializeApp } from "firebase/app";

import { getAuth } from "firebase/auth";



const firebaseConfig = {
  apiKey: "AIzaSyALp4tieqYoQMX_9etVodQthBoMRvyjyT4",
  authDomain: "crud-login-417a8.firebaseapp.com",
  projectId: "crud-login-417a8",
  storageBucket: "crud-login-417a8.appspot.com",
  messagingSenderId: "38084177635",
  appId: "1:38084177635:web:d74e2d77c3229c0fe31fd6",
  measurementId: "G-XKYHP4LZG0"
};


const app = initializeApp(firebaseConfig);

const auth = getAuth(app);


export { auth};
